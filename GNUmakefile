## GNUmakefile --- make file for a BuildStream workspace.

# Copyright (C) 2020 Ralph Schleicher

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in
#      the documentation and/or other materials provided with the
#      distribution.
#
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived
#      from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

## Code:

%:: %.in
	sed \
	-e "s;@HOME@;$$PWD;g" \
	$< > $@~ && mv -f $@~ $@
	if test -x $< ; then chmod +x $@ ; fi

.PHONY: default
default:
	@echo
	@echo "Important make targets:"
	@echo
	@echo "all                 Prepare workspace."
	@echo "clean               Remove built files."
	@echo "sync                Push changes to GitLab."
	@echo

.PHONY: all
all: BuildStream.desktop startup.sh .profile

.PHONY: clean
clean:
	rm -f BuildStream.desktop startup.sh .profile

.PHONY: sync
sync:
	cd ~ralph/src/gitlab && ./gitlab.sh buildstream-workspace

.PHONY: README
README: README.html
README.html: README.md
	markdown $< > $@

## GNUmakefile ends here
